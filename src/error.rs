/// Convience type
pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    /// Io Error
    Io(std::io::Error),
    /// Reqwest Error
    ReqwestError(reqwest::Error),
    /// Lofty
    LoftyError(lofty::error::LoftyError),

    /// Lyrics fetching error
    LyricsFetchError(String),
    /// Song Error
    SongError(String),
}

impl From<lofty::error::LoftyError> for Error {
    fn from(v: lofty::error::LoftyError) -> Self {
        Self::LoftyError(v)
    }
}

impl std::error::Error for Error {}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::Io(e) => e.fmt(f),
            Error::ReqwestError(e) => e.fmt(f),
            Error::LoftyError(e) => e.fmt(f),
            Error::LyricsFetchError(e) => e.fmt(f),
            Error::SongError(e) => e.fmt(f),
        }
    }
}

impl From<reqwest::Error> for Error {
    fn from(v: reqwest::Error) -> Self {
        Self::ReqwestError(v)
    }
}
