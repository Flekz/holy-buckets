use color_eyre::Result;
use holy_buckets::lyrics::fetch_lyrics;
use std::env;

#[tokio::main]
async fn main() -> Result<()> {
    let fnction = "mpd";
    // Fetch lyrics
    match fnction {
        "fetch_lyrics" => {
            let args: Vec<String> = env::args().collect();
            fetch_lyrics(args).await?;
        }
        "mpd" => {
            holy_buckets::mpd::stuff().await?;
        }
        f => unimplemented!("{f}"),
    }

    Ok(())
}
