use crate::error::{Error, Result};
use lofty::prelude::*;
use lofty::probe::Probe;
use lofty::tag::Tag;
use lofty::{config::WriteOptions, file::TaggedFile};
use std::path::{Path, PathBuf};
use walkdir::WalkDir;

use serde::Deserialize;

pub struct Song {
    pub title: String,
    pub artist: String,
    pub album: String,
    /// Duration in seconds
    pub duration: f32,
    pub context: TaggedFile,
    pub path: PathBuf,
}

impl Song {
    pub fn new(path: &Path) -> Result<Self> {
        let mut tagged_file = Probe::open(path)?.read()?;
        let props = tagged_file.properties();
        let duration = props.duration().as_secs_f32();

        // Get or set primary tag
        let tag = match tagged_file.primary_tag_mut() {
            Some(primary_tag) => primary_tag,
            None => {
                if let Some(first_tag) = tagged_file.first_tag_mut() {
                    first_tag
                } else {
                    let tag_type = tagged_file.primary_tag_type();

                    eprintln!("WARN: No tags found, creating a new tag of type `{tag_type:?}`");
                    tagged_file.insert_tag(Tag::new(tag_type));
                    tagged_file.primary_tag_mut().unwrap()
                }
            }
        };

        let artist = tag.artist();
        println!("{:?}", artist);

        Ok(Self {
            title: tag.title().unwrap().into(),
            artist: tag.artist().unwrap().into(),
            album: tag.album().unwrap().into(),
            duration,
            context: tagged_file,
            path: path.into(),
        })
    }

    pub async fn fetch_lyrics(&mut self) -> Result<()> {
        let tags = self.context.primary_tag_mut().unwrap();
        if tags.get(&ItemKey::Lyrics).is_some() {
            println!("Lyrics already exists for {}", self.title);
            return Ok(());
        }
        let lyrics = Lyrics::new(&self.title, &self.artist, &self.album, self.duration).await?;

        // TODO: Is this bugged? E.G. Lupe Fiasco - Alan Forever downloaded plain lyrics but synced lyrics is available.
        if let Some(synced_lyrics) = lyrics.synced_lyrics {
            tags.insert_text(ItemKey::Lyrics, synced_lyrics);
        } else if let Some(plain_lyrics) = lyrics.plain_lyrics {
            tags.insert_text(ItemKey::Lyrics, plain_lyrics);
        }

        Ok(())
    }

    pub fn print_tags(&self) {
        let tags = self.context.primary_tag().unwrap();
        for items in tags.items() {
            println!("{:?} - {:?}", items.key(), &items.value())
        }
    }

    pub fn save_file(self) -> Result<()> {
        self.context
            .save_to_path(self.path, WriteOptions::default())?;
        Ok(())
    }
}

#[allow(dead_code)]
#[derive(Debug, Deserialize, Clone)]
pub struct Lyrics {
    pub id: usize,
    pub name: String,
    #[serde(rename = "trackName")]
    pub track_name: String,
    #[serde(rename = "albumName")]
    pub album_name: String,
    pub duration: f32,
    pub instrumental: bool,
    #[serde(rename = "plainLyrics")]
    pub plain_lyrics: Option<String>,
    #[serde(rename = "syncedLyrics")]
    pub synced_lyrics: Option<String>,
}

impl Lyrics {
    pub async fn new(title: &str, artist: &str, album: &str, duration: f32) -> Result<Self> {
        let client = reqwest::Client::new();
        let header = "Holy Buckets https://gitlab.com/Flekz/holy-buckets";

        let response = client
            .get("https://lrclib.net/api/get?")
            .header(reqwest::header::USER_AGENT, header)
            .query(&[
                ("track_name", title),
                ("artist_name", artist),
                ("album_name", album),
                ("duration", &format!("{}", duration)),
            ])
            .send()
            .await?;

        // OK status
        if response.status() == 200 {
            let lyrics: Lyrics = response.json().await?;
            return Ok(lyrics);
        }

        println!("Lyrics not matched, trying wider search");
        println!("Searching: {title} - {artist}");
        let res: Vec<Lyrics> = client
            .get("https://lrclib.net/api/search?")
            .header(reqwest::header::USER_AGENT, header)
            .query(&[("track_name", title), ("artist_name", artist)])
            .send()
            .await?
            .json()
            .await?;

        println!("{:#?}", res);

        if let Some(v) = res.iter().find(|v| (v.duration - duration).abs() < 5.0) {
            Ok(v.clone())
        } else {
            Err(Error::LyricsFetchError("Failed to fetch error".to_owned()))
        }
    }
}

pub async fn fetch_lyrics(args: Vec<String>) -> color_eyre::Result<()> {
    if args.len() < 2 {
        eprintln!("Usage: {} <directory1> <directory2> ...", args[0]);
        panic!();
    }

    for dir in &args[1..] {
        println!("Processing directory: {}", dir);
        // Walk through the directory and its subdirectories
        for entry in WalkDir::new(dir).into_iter().filter_map(|e| e.ok()) {
            let path = entry.path();
            if path.is_file() {
                match path.extension() {
                    Some(ext) => match ext.to_str() {
                        Some("opus") | Some("mp3") => match Song::new(path) {
                            Ok(mut song) => {
                                println!("{} - {} - {}", song.title, song.artist, song.album);
                                if let Err(e) = song.fetch_lyrics().await {
                                    println!("Error fetching lyrics {e}")
                                };

                                if let Err(e) = song.save_file() {
                                    println!("Errr writing file {e}");
                                };
                            }
                            Err(e) => println!("Error fetching song info {e}"),
                        },
                        Some(extension) => {
                            println!("Skipping file {path:?} with extension {extension}")
                        }

                        None => (),
                    },
                    None => (),
                }
            }
        }
    }
    Ok(())
}
