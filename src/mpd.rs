use color_eyre::Result;
use mpd_client::{
    client::{ConnectionEvent, ConnectionEvents, Subsystem},
    commands, Client,
};
use tokio::net::{TcpStream, UnixStream};

async fn connect_mpd() -> Result<(Client, ConnectionEvents)> {
    if true {
        // TCP
        let connection = TcpStream::connect("localhost:6600").await?;
        let (client, state_changes) = Client::connect(connection).await?;
        Ok((client, state_changes))
    } else {
        // Unix socket
        let connection = UnixStream::connect("/run/user/1000/mpd").await?;
        let (client, state_changes) = Client::connect(connection).await?;
        Ok((client, state_changes))
    }
}

pub async fn stuff() -> Result<()> {
    let (client, mut state_changes) = connect_mpd().await?;

    'outer: loop {
        match client.command(commands::CurrentSong).await? {
            Some(song_in_queue) => {
                println!(
                    "\"{}\" by \"{}\"",
                    song_in_queue.song.title().unwrap_or(""),
                    song_in_queue.song.artists().join(", "),
                );
            }
            None => println!("(none)"),
        }
        let status = client.command(commands::Status).await?;
        if let Some(duration) = status.elapsed {
            println!("{:?}", duration);
        }

        loop {
            // Wait for state change
            match state_changes.next().await {
                Some(ConnectionEvent::SubsystemChange(Subsystem::Player)) => break, // Relevant change
                Some(ConnectionEvent::SubsystemChange(change)) => println!("{change:?}"), // Irrelevant change
                _ => break 'outer, // connection closed
            }
        }
    }
    Ok(())
}
